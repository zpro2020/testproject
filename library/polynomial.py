class Polynomial:
    def __init__(self, *coefs):
        self.coefs = coefs

    def __add__(self, other):
        return Polynomial(*(i + j for i, j in zip(self.coefs, other.coefs)))

    def __repr__(self):
        return self._print()

    def __str__(self):
        return self._print()

    def __len__(self):
        return len(self.coefs) - 1

    def print(self):
        print(self._print())

    def _print(self):
        s = ""
        for ind, coef in enumerate(self.coefs):
            if ind == 0:
                s += f"{coef} + "
            elif ind == 1:
                s += f"{coef}x + "
            elif ind == len(self.coefs) - 1:
                s += f"{coef}x^{ind}"
            else:
                s += f"{coef}x^{ind} + "

        return s
