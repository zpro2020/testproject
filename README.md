# Example project
This is an example GitLab project.

## GitLab
GitLab is a platform for source code version control
using [git](https://git-scm.com/).
Registering is easy using a google account. 

### HTTPS vs SSH
There are two ways to authenticate with GitLab, HTTPS which 
uses a password or SSH which uses a private key.
Setting up a password is done in `Account -> Settings -> Passwords`.

Using SSH is preferable as it is more secure. 
A comprehensive resource on SSH and GitLab can be found [here](https://docs.gitlab.com/ce/ssh/).

[TL;DR](https://confluence.atlassian.com/bitbucketserver/creating-ssh-keys-776639788.html)
Create an SSH certificate-key pair and add the SSH certificate to your GitLab
account in `Settings -> SSH`. 

To get a project you need to clone it from GitLib. Hit the blue `Clone` button
and chose the appropriate method (after setup, SSH should work without asking for a password).
Copy the URL and got to `PyCharm -> VCS -> Get from version control` and paste the URL.
The projects are now linked, you can use `pull/push` git actions.

## Python version and dependency management
Note: These tools are not necessary, they are recommendations.
To manage versions of python, one can use a tool such as [pyenv](https://github.com/pyenv/pyenv) which 
can install and switch between different versions of the Python language

Python uses [pip](https://pypi.org/project/pip/) for its dependency management.
There is a nice wrapper for pip which adds functionality and intelligent dependency resolving, namely [poetry](https://python-poetry.org/docs/)
Poetry creates a virtual environment for each python project. This means that the dependencies for each priject are separated.
This project uses poetry, to create a virtual environment run `poetry install` from a console.

If you use a virtual environment for python, you may need to tell PyCharm, `Settings -> Project -> Python interpreter`
choose existing one, and set the path to the python binary in the virtual enironment, e.g. `/Users/peter/Library/Caches/pypoetry/virtualenvs/testproject-JQHyqQ08-py3.8/bin/python3`
This path is shown when poetry creates a virtual environment, or by running `poetry show -v`.

# Lecture links
Links to videos of ZPRO 2020:
- [30.09.](https://youtu.be/ZSueOTmMyUQ)
- [02.10.](https://youtu.be/NNYsWFUPpXw)
- [07.10](https://youtu.be/28WjerjIDU8)
- [09.10](https://youtu.be/ax5BAHnd9pk)
- [14.10](https://youtu.be/PFYx5HFbAP8)
- [16.10](https://youtu.be/Sxqx9s0jWYA)
- [21.10](https://youtu.be/d4gk4CwCeUY)
- [23.10.](https://youtu.be/kaKY5Tny4Zo)
- [04.11.](https://youtu.be/vl-LPvVBogU)
- [06.11.](https://youtu.be/I6RO1bLnvfM)
- [11.11](https://youtu.be/fUeffxWAPMw)
- [13.11.](https://youtu.be/VtlQrn01Z_U)
- [18.11.](https://youtu.be/NyvNcwOcRh4)
- [20.11.](https://youtu.be/K4hv3hJ-few)
- [25.11.](https://youtu.be/ECIq4MxKTTU)
- [27.11.](https://youtu.be/WYUNZLKJkAI)
- [02.12.](https://youtu.be/qOhAGmSFc_U)
- [04.12.](https://youtu.be/Yuj0KezBKJU)
- [09.12.](https://youtu.be/EYFvuwuyseo)
- [11.12.](https://youtu.be/8P9E4gn3hAU)
- [16.12.](https://youtu.be/SDouvnlw0F4)
- [18.12.](https://youtu.be/DPGvB5MgvNc)
