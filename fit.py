import matplotlib.pyplot as plt
import numpy as np

with open("data.txt", "r") as f:
    # data = f.readlines()
    data = [line.strip('\n') for line in f]

x = []
y = []
for point in data:
    a, b = point.split(' ')
    x.append(float(a))
    y.append(float(b))

coefs = np.polyfit(x, y, 1)
poly1d = np.poly1d(coefs)

plt.plot(x, y, 'yo', x, poly1d(x), '--k')
plt.show()
# plt.savefig("fig.png")
