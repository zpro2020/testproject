from library.polynomial import Polynomial

l = [3, 2, 1]
t = (4, 5, 6)
d = {"k1": 1, "k2": 2}

p1 = Polynomial(1, 2, 3)
p2 = Polynomial(2, 4, 6)
p3 = p1 + p2
p2.print()
p3.print()
